intribe-logger-lib
====================

Consistent logging and formatting for all intribe applications

## About

This library provides common logging methods e.g. trace, debug, info, error etc
and enables a basic template/log format to be specified. So that all intribe
applications can be configured to produce the same log output for a collective
logging service - such as stackdriver or splunk.


## Usage

```javascript
import {Logger} from './Logger';

let logger = new Logger({});

logger.trace('trace message');
logger.debug('debug message');
logger.info('info message');
logger.warning('warning message');
logger.error('error message');
logger.fatal('fatal message');

```

### Configuration

There are some minimal configuration options (see /src/ILoggerOptions.ts) which 
have some sane defaults set (see src/Defaults.ts)

```javascript
import {Logger} from './Logger';
import {LogLevels} from './LogLevels';

let options = {
  appName: 'intribe-api',
  logFormat: '%appName% %logLevelEmoji% %logLevel% %message%',
  outputLogLevels: [
    LogLevels.INFO,
    LogLevels.WARNING,
    LogLevels.ERROR,
    LogLevels.FATAL,
  ],
  outputFunction: console.log,
};

let logger = new Logger(options);
```


## Building and testing

Builds compile in to the dist dir, the entry point being dist/index.js.
Builds should automatically occur during a git commit.

### Testing src code

You can run tests against the typescript code when developing with:
 
```bash
npm test
```

### Building dist code

You can build the distribution code with:

```bash
npm run build
```

Note that build will also run mocha (tests) over the built files

### Testing build dist code

You can run just the tests after building the dist code by running

```bash
npm run build:test
```

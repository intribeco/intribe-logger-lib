import {LogLevels} from './LogLevels';

export interface ILoggerOptions {
  // The name of the application doing the logging
  appName?: string;
  // The output format of the log (@see Substitutions.ts)
  logFormat?: string;
  // The list of log levels which should be output, levels
  // not listed here will not be output
  outputLogLevels?: LogLevels[];
  // The function which receives the formatted log message
  // and is responsible for outputting the message (or
  // sending it onwards)
  outputFunction?(message: string): void;
}

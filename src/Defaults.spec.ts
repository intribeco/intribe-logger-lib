import * as chai from 'chai';

const expect = chai.expect;
import 'mocha';

import {Defaults} from './Defaults';

describe('Defaults.ts', () => {
  describe('basic defaults', () => {
    it('should get basic defaults', () => {
      const defaults = Defaults.getDefaults();

      expect(defaults).to.have.property('appName', 'App name not set');
      expect(defaults).to.have.property('logFormat', '%appName% %logLevelEmoji% %logLevel% %message%');
      expect(defaults).to.have.property('outputLogLevels')
        .to.have.members([2, 3, 4, 5])
        .and.to.not.have.members([0, 1]);
      expect(defaults).to.have.property('outputFunction').instanceOf(Function);
    });
  });

  describe('defaults considering env variable', () => {
    let env: any;

    before(() => {
      env = Object.assign({}, process.env);
    });

    it('should get defaults considering env variable.', () => {
      process.env.INTRIBE_LOGGER_LOG_LEVELS = 'INFO ERROR FATAL';

      const defaults = Defaults.getDefaults();

      expect(defaults).to.have.property('appName', 'App name not set');
      expect(defaults).to.have.property('logFormat', '%appName% %logLevelEmoji% %logLevel% %message%');
      expect(defaults).to.have.property('outputLogLevels')
        .to.have.members([2, 4, 5])
        .and.to.not.have.members([0, 1, 3]);
      expect(defaults).to.have.property('outputFunction').instanceOf(Function);
    });

    after(() => {
      process.env = env;
    });
  });
});

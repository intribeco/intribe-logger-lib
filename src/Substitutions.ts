export enum Substitutions {
  // The name of the application
  appName,
  // an EMOJI which matches the log level
  logLevelEmoji,
  // The log level string (INFO, ERROR etc)
  logLevel,
  // The log message
  message,
}

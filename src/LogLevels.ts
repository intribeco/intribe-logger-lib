export enum LogLevels {
  TRACE,
  DEBUG,
  INFO,
  WARNING,
  ERROR,
  FATAL,
}

let emojiMap = {};

emojiMap[LogLevels.TRACE] = '👁‍';
emojiMap[LogLevels.DEBUG] = '🐞';
emojiMap[LogLevels.INFO] = 'ℹ️';
emojiMap[LogLevels.WARNING] = '⚠️️';
emojiMap[LogLevels.ERROR] = '‼️';
emojiMap[LogLevels.FATAL] = '☠️️';

export const LogLevelEmoji = emojiMap;

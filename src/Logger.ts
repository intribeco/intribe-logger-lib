import {ILoggerOptions} from './ILoggerOptions';
import {Defaults} from './Defaults';
import {Substitutions} from './Substitutions';
import {LogLevelEmoji, LogLevels} from './LogLevels';

export class Logger {
  public constructor(public options: ILoggerOptions) {
    this.setDefaults();
  }

  public trace(message = ''): Logger {
    this.output(LogLevels.TRACE, message);
    return this;
  }

  public debug(message = ''): Logger {
    this.output(LogLevels.DEBUG, message);
    return this;
  }

  public info(message = ''): Logger {
    this.output(LogLevels.INFO, message);
    return this;
  }

  public warning(message = ''): Logger {
    this.output(LogLevels.WARNING, message);
    return this;
  }

  public error(message = ''): Logger {
    this.output(LogLevels.ERROR, message);
    return this;
  }

  public fatal(message = ''): Logger {
    this.output(LogLevels.FATAL, message);
    return this;
  }

  private getSubstitutionMap(message: string, logLevel: number): object {
    let subsMap = {};

    subsMap[Substitutions.message] = message;
    subsMap[Substitutions.logLevel] = LogLevels[logLevel];
    subsMap[Substitutions.logLevelEmoji] = LogLevelEmoji[logLevel];
    subsMap[Substitutions.appName] = this.options.appName;

    return subsMap;
  }

  private formatOutput(subsMap: object): string {
    let message = this.options.logFormat + '';

    Object.keys(Substitutions)
      .filter((value: any) => { return !isNaN(value); })
      .map((key) => {
        if (!subsMap[key]) {
          return;
        }

        let name = Substitutions[key];

        message = message.replace(new RegExp(`%${name}%`), subsMap[key]);
      });

    return message;
  }

  private output(logLevel: number, message: string): void {
    // Only log the levels specified in the options
    if (this.options.outputLogLevels.indexOf(logLevel) === -1) {
      return;
    }

    this.options.outputFunction(
      this.formatOutput(
        this.getSubstitutionMap(message, logLevel),
      ),
    );
  }

  private setDefaults(): void {
    const defaults = Defaults.getDefaults();

    this.options = this.options || {
      appName: defaults.appName,
    };

    this.options.outputFunction =
      this.options.outputFunction || defaults.outputFunction;

    this.options.logFormat =
      this.options.logFormat || defaults.logFormat;

    this.options.outputLogLevels =
      this.options.outputLogLevels || defaults.outputLogLevels;

    this.options.appName =
      this.options.appName || defaults.appName;

  }
}

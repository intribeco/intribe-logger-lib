import * as chai from 'chai';
const expect = chai.expect;
import 'mocha';

import {Logger} from './Logger';
import {ILoggerOptions} from './ILoggerOptions';
import {LogLevelEmoji, LogLevels} from './LogLevels';

describe('Logger.ts', () => {
  describe('construction', () => {
    it('must be an instance of the Logger class', () => {
      let options: ILoggerOptions = {
        appName: 'test-cases',
      };

      expect(new Logger(options))
        .to.be.instanceOf(Logger);
    });
  });

  describe('instance methods', () => {

    describe('selective log level output', () => {
      let mutipleOutput: string[] = [];

      beforeEach(() => {
        mutipleOutput = [];
      });

      const outputFunction = (message: string) => {
        mutipleOutput.push(message);
      };

      let options = {
        appName: 'test-cases',
        logFormat: '%message%',
        outputLogLevels: [LogLevels.INFO],
        outputFunction: outputFunction,
      };

      it('must output only for single selected level', () => {
        let logger = new Logger(options);

        logger.info('info message');
        logger.debug('debug message');
        logger.trace('trace message');

        expect(mutipleOutput)
          .to.have.members(['info message'])
          .and.to.not.have.members(['trace message', 'debug message']);

      });

      it('must output only for multiple selected level', () => {
        options.outputLogLevels = [
          LogLevels.INFO,
          LogLevels.WARNING,
          LogLevels.ERROR,
          LogLevels.FATAL,
        ];

        let logger = new Logger(options);

        logger.info('info message');
        logger.warning('warning message');
        logger.error('error message');
        logger.fatal('fatal message');
        logger.debug('debug message');
        logger.trace('trace message');

        expect(mutipleOutput)
          .to.have.members(['info message', 'warning message', 'error message', 'fatal message'])
          .and.to.not.have.members(['trace message', 'debug message']);

      });

      it('must output only for multiple selected level (inverted)', () => {
        options.outputLogLevels = [
          LogLevels.TRACE,
          LogLevels.DEBUG,
        ];

        let logger = new Logger(options);

        logger.info('info message');
        logger.warning('warning message');
        logger.error('error message');
        logger.fatal('fatal message');
        logger.debug('debug message');
        logger.trace('trace message');

        expect(mutipleOutput)
          .to.have.members(['trace message', 'debug message'])
          .and.not.to.have.members(['info message', 'warning message', 'error message', 'fatal message']);

      });
    });

    describe('basic logging coverage for each method', () => {
      let logLevels = Object.keys(LogLevels)
        .filter((key: any) => !isNaN(key));

      logLevels.forEach((_index: string) => {
        let logLevelIndex = parseInt(_index, 10);
        let logLevel = LogLevels[logLevelIndex];
        let logMethodName = logLevel.toLowerCase();

        describe(`Logger.${LogLevels[logLevelIndex].toLowerCase()}()`, () => {
          let output: string;

          beforeEach(() => {
            output = 'unset';
          });

          describe('basic logging', () => {

            const outputFunction = (message: string) => {
              output = message;
            };
            let options = {
              appName: 'TEST-APP',
              logFormat: '%message%',
              outputLogLevels: [logLevelIndex],
              outputFunction: outputFunction,
            };

            let logger = new Logger(options);

            it('must be a function', () => {
              expect(logger[logMethodName])
                .to.be.a('function');
            });

            it('must output a customised format', () => {
              let testMessage = 'Test Message';

              logger[logMethodName](testMessage);

              expect(output).to.equal(testMessage);

            });
          });

          describe('custom log format', () => {

            const outputFunction = (message: string) => {
              output = message;
            };

            let options = {
              appName: 'TEST-APP',
              logFormat: '%appName% %logLevelEmoji% %logLevel% %message%',
              outputLogLevels: [logLevelIndex],
              outputFunction: outputFunction,
            };

            let logger = new Logger(options);

            it('must send the appropriate string', () => {
              let testMessage = 'Test Message';

              logger[logMethodName](testMessage);

              expect(output).to.equal(`${options.appName} ${LogLevelEmoji[logLevelIndex]} ${logLevel} Test Message`);
            });
          });
        });
      });
    });
  });
});

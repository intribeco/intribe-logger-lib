let gulp = require('gulp4');
let gulpRun = require('gulp-run');
let babel = require('gulp-babel');
let del = require('del');


let paths = {
  scripts: {
    src: 'src/**/*.ts',
    dest: 'src/'
  }
};

function testTsMocha() {
  "use strict";
  try {
    return gulpRun('npm run clean && npm test').exec();
  } catch (e) {
    console.log(e);
  }
  return true;
}

function buildTest() {
  "use strict";
  return gulpRun('npm run build:test').exec();

}

function watch() {
  "use strict";
  gulp.watch(paths.scripts.src, testTsMocha);
}

function buildCompile() {
  "use strict";

  return gulp.src("src/**/*.ts")
    .pipe(babel())
    //.pipe(concat('index.js'))
    .pipe(gulp.dest("dist"));
}

function buildClean() {
  "use strict";
  return del([
    'dist/**/*',
  ]);
}

/*
 * Define default task that can be called by just running `gulp` from cli
 */
gulp.task('test:watch', watch);
gulp.task('build:compile', buildCompile);
gulp.task('build:clean', buildClean);
gulp.task('build:test', buildTest);
gulp.task('build', gulp.series('build:clean', 'build:compile', 'build:test'));

import {LogLevels} from './LogLevels';
import {ILoggerOptions} from './ILoggerOptions';

export class Defaults {

  public static getDefaults(): ILoggerOptions {
    return {
      appName: 'App name not set',
      logFormat: '%appName% %logLevelEmoji% %logLevel% %message%',
      outputLogLevels: Defaults.getDefaultLogLevels(),
      outputFunction: console.log,
    };
  }

  private static getDefaultLogLevels(): number[] {
    const envLogLevels = (process.env.INTRIBE_LOGGER_LOG_LEVELS || 'INFO WARNING ERROR FATAL').split(' ');

    const result: number [] = [];

    Object.keys(LogLevels)
      .filter(key => !isNaN(Number(LogLevels[key])))
      .forEach((value) => {
        if (envLogLevels.indexOf(value) !== -1) {
          result.push(LogLevels[value]);
        }
      });

    return result;
  }
}

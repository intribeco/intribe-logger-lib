import * as chai from 'chai';
const expect = chai.expect;
import 'mocha';

import {Logger} from './index';
import {Logger as LoggerClass} from './Logger';

describe('index.ts', () => {
  it('must instantiate a logger', () => {
    let options = {
      appName: 'test-app',
    };

    expect(Logger(options))
      .to.be.instanceOf(LoggerClass);
  });

  it('must log something', () => {
    let output: string;
    let options = {
      appName: 'test-app',
      logFormat: '%message%',
      outputFunction: (inputMessage: string) => { output = inputMessage; },
    };

    let message = 'test message output';
    let logger = Logger(options);

    logger.info(message);

    expect(output).to.equal(message);
  });
});

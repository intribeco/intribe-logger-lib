import {ILoggerOptions} from './ILoggerOptions';
import {LogLevels as LoggerLevels} from './LogLevels';
import {Logger as LoggerClass} from './Logger';

class Factory {
  public static create(options: ILoggerOptions): LoggerClass {
    return new LoggerClass(options);
  }
}

export type LoggerOptions = ILoggerOptions;
export const LogLevels = LoggerLevels;
export const Logger = (options: LoggerOptions) => Factory.create(options);
